

########## VNET & ALL SUBNETS ################
variable "environment" {
  description = "environment value"
  default     = "dev"

}
variable "gateway_name" {
  description = "team value"
  default     = "dev-natgateway"

}

variable "team" {
  description = "team value"
  default     = "cot"

}

##
variable "owner" {
  description = "owner"
  default     = "kira.kota"

}

variable "owneremail" {
  description = "owneremail"
  default     = "kiran.kota@phenompeople.com"

}

variable "creationdate" {
  description = "creationdate"
  default     = "22feb2023"

}

variable "renewaldate" {
  description = "renewaldate"
  default     = "22feb2024"

}

variable "reason" {
  description = "reason"
  default     = "new infra setup"

}

variable "component" {
  description = "component"
  default     = "vnet"

}

variable "module" {
  description = "module"
  default     = "vnet"

}

##

variable "vnet_name" {
  description = "The resource group where the VNet will be created"
  default     = "dev"

}

variable "vnet_resource_group" {
  description = "The resource group where the VNet will be created"
  default     = "dev"

}

variable "vnet_location" {
  description = "The location VNet will be created"
  default     = "East US"

}

variable "vnet_address_space" {
  description = "The location VNet will be created"
  default     = ["10.100.0.0/16"]

}

variable "public-subnet1" {
  description = "The public-subnet1 CIDR VNet will be created"
  default     = ["10.100.4.0/22"]

}
variable "public-subnet2" {
  description = "The The public-subnet2 CIDR will be created"
  default     = ["10.100.8.0/22"]

}

variable "public-subnet3" {
  description = "The The public-subnet3 CIDR will be created"
  default     = ["10.100.12.0/22"]

}

variable "public-subnet4" {
  description = "The The public-subnet4 CIDR will be created"
  default     = ["10.100.14.0/22"]

}


variable "private-subnet1" {
  description = "The private-subnet1 CIDR VNet will be created"
  default     = ["10.100.16.0/22"]

}
variable "private-subnet2" {
  description = "The The private-subnet2 CIDR will be created"
  default     = ["10.100.20.0/22"]

}

variable "private-subnet3" {
  description = "The The public-subnet3 CIDR will be created"
  default     = ["10.100.24.0/22"]

}

variable "private-subnet4" {
  description = "The The private-subnet2 CIDR will be created"
  default     = ["10.100.25.0/22"]

}

variable "private-subnet5" {
  description = "The The public-subnet3 CIDR will be created"
  default     = ["10.100.26.0/22"]

}

variable "private-subnet6" {
  description = "The The public-subnet3 CIDR will be created"
  default     = null

}

variable "private-subnet7" {
  description = "The The public-subnet3 CIDR will be created"
  default     = null
}

variable "private-subnet8" {
  description = "The The private-subnet8 CIDR will be created"
  default     = null
}
variable "private-subnet9" {
  description = "The The private-subnet9 CIDR will be created"
  default     = null
}
variable "private-subnet10" {
  description = "The The private-subnet10 CIDR will be created"
  default     = null
}
variable "public-subnet-nsg" {
  description = "public subnet nsg"
  default     = ""

}

variable "private-subnet-nsg" {
  description = "private subnet nsg"
  default     = ""

}

variable "natgateway_publicip" {
  description = "private subnet nsg"
  default     = ""

}
variable "natgateway_publicip_prefix" {
  description = "private subnet nsg"
  default     = ""

}

variable "default_service_endpoints" {
  type    = list(string)
  default = ["", ""]  # Add all your default service endpoints here
}
################### VNET END LINE ###################
variable "subnet_names" {
  description = "List of subnet names"
  type        = list(string)
  default     = ["subnet50", "subnet51", "subnet52"]
}

variable "subnet_cidr" {
  description = "CIDR blocks for subnets"
  type        = list(string)
  default     = ["", "", ""]
}

variable "nat2_gateway_id " {
  type=string
  default=""
  
}