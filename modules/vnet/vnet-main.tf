terraform {
  required_version = ">= 1.3.9"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.42.0"
    }
  }

}
provider "azurerm" {
  features {}
}

resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  address_space       = var.vnet_address_space
  resource_group_name = var.vnet_resource_group
  location            = var.vnet_location
  tags = {
    env          = var.environment
    team         = var.team
    owner        = var.owner
    owneremail   = var.owneremail
    creationdate = var.creationdate
    renewaldate  = var.renewaldate
    reason       = var.reason
    component    = var.component
    module       = var.module
  }
}


#################PUBLIC SUBNETS#############33
resource "azurerm_subnet" "subnet1" {
  name                 = "public-subnet1"
  resource_group_name  = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.public-subnet1
  
}

resource "azurerm_subnet" "subnet2" {
  name                 = "public-subnet2"
  resource_group_name  = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.public-subnet2
  
}


resource "azurerm_subnet" "subnet3" {
  name                 = "public-subnet3"
  resource_group_name  = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.public-subnet3
  
}

resource "azurerm_subnet" "subnet4" {
  count                = var.public-subnet4 == null ? 0 : 1
  name                 = "appgw-public-subnet4"
  resource_group_name  = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.public-subnet4
  
}
resource "azurerm_network_security_group" "nat" {
  name                = var.public-subnet-nsg
  location            = var.vnet_location
  resource_group_name = var.vnet_resource_group
  tags = {
    env          = var.environment
    team         = var.team
    owner        = var.owner
    owneremail   = var.owneremail
    creationdate = var.creationdate
    renewaldate  = var.renewaldate
    reason       = var.reason
    component    = var.component
    module       = var.module
  }
}

resource "azurerm_network_security_rule" "nat_https" {
  name                        = "Allow_HTTPS"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = var.vnet_resource_group
  network_security_group_name = azurerm_network_security_group.nat.name
}
resource "azurerm_network_security_rule" "nat_http" {
  name                        = "Allow_HTTP"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = var.vnet_resource_group
  network_security_group_name = azurerm_network_security_group.nat.name
}

resource "azurerm_subnet_network_security_group_association" "subnet1" {
  subnet_id                 = azurerm_subnet.subnet1.id
  network_security_group_id = azurerm_network_security_group.nat.id
}

resource "azurerm_subnet_network_security_group_association" "subnet2" {
  subnet_id                 = azurerm_subnet.subnet2.id
  network_security_group_id = azurerm_network_security_group.nat.id
}

resource "azurerm_subnet_network_security_group_association" "subnet3" {
  subnet_id                 = azurerm_subnet.subnet3.id
  network_security_group_id = azurerm_network_security_group.nat.id
}

resource "azurerm_subnet_network_security_group_association" "subnet4" {
  subnet_id                 = azurerm_subnet.subnet4[0].id
  network_security_group_id = azurerm_network_security_group.nat.id
}


###########PRIVATE SUBNETS##############
resource "azurerm_subnet" "pr-subnet1" {
  name                                           = "private-subnet1"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet1
  enforce_private_link_endpoint_network_policies = true
  
}

resource "azurerm_subnet" "pr-subnet2" {
  name                                           = "private-subnet2"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet2
  enforce_private_link_endpoint_network_policies = true
  
}


resource "azurerm_subnet" "pr-subnet3" {
  name                                           = "private-subnet3"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet3
  enforce_private_link_endpoint_network_policies = true
  
}

resource "azurerm_subnet" "pr-subnet4" {
  name                 = "private-subnet4"
  resource_group_name  = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.private-subnet4
  
  delegation {
    name = "delegation"

    service_delegation {
      name    = "Microsoft.DBforPostgreSQL/flexibleServers"
      actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
    }
  }
  enforce_private_link_endpoint_network_policies = true
}


resource "azurerm_subnet" "pr-subnet5" {
  name                                           = "private-subnet5"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet5
  enforce_private_link_endpoint_network_policies = true
  
}

resource "azurerm_subnet" "pr-subnet6" {
  count                                          = var.private-subnet6 == null ? 0 : 1
  name                                           = "private-subnet6"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet6
  enforce_private_link_endpoint_network_policies = true
  
}

resource "azurerm_subnet" "pr-subnet7" {
  count                                          = var.private-subnet7 == null ? 0 : 1
  name                                           = "private-subnet7"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet7
  enforce_private_link_endpoint_network_policies = true
  
}
resource "azurerm_subnet" "pr-subnet8" {
  count                                          = var.private-subnet8 == null ? 0 : 1
  name                                           = "private-subnet8"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet8
  enforce_private_link_endpoint_network_policies = true
  
}
resource "azurerm_subnet" "pr-subnet9" {
  count                                          = var.private-subnet9 == null ? 0 : 1
  name                                           = "appgw-private-subnet9"
  resource_group_name                            = var.vnet_resource_group
  virtual_network_name                           = azurerm_virtual_network.vnet.name
  address_prefixes                               = var.private-subnet9
  enforce_private_link_endpoint_network_policies = true
  
}
resource "azurerm_subnet" "pr-subnet10" {
  count                = var.private-subnet10 == null ? 0 : 1
  name                 = "hdinsights-private-subnet10"
  resource_group_name  = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.private-subnet10
  

}
resource "azurerm_network_security_group" "pr-nat" {
  name                = var.private-subnet-nsg
  location            = var.vnet_location
  resource_group_name = var.vnet_resource_group
  tags = {
    env          = var.environment
    team         = var.team
    owner        = var.owner
    owneremail   = var.owneremail
    creationdate = var.creationdate
    renewaldate  = var.renewaldate
    reason       = var.reason
    component    = var.component
    module       = var.module
  }
}

resource "azurerm_network_security_rule" "nat_ssh" {
  name                        = "Allow_SSH"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "172.18.1.5/32"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = var.vnet_resource_group
  network_security_group_name = azurerm_network_security_group.pr-nat.name
}
resource "azurerm_network_security_rule" "allow_any" {
  name                        = "AllowAnyCustomAnyOutbound"
  priority                    = 111
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = var.vnet_resource_group
  network_security_group_name = azurerm_network_security_group.pr-nat.name
}
resource "azurerm_subnet_network_security_group_association" "pr-subnet1" {
  subnet_id                 = azurerm_subnet.pr-subnet1.id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_network_security_group_association" "pr-subnet2" {
  subnet_id                 = azurerm_subnet.pr-subnet2.id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_network_security_group_association" "pr-subnet3" {
  subnet_id                 = azurerm_subnet.pr-subnet3.id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_network_security_group_association" "pr-subnet4" {
  subnet_id                 = azurerm_subnet.pr-subnet4.id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_network_security_group_association" "pr-subnet5" {
  subnet_id                 = azurerm_subnet.pr-subnet5.id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_network_security_group_association" "pr-subnet6" {
  count                     = var.private-subnet6 == null ? 0 : 1
  subnet_id                 = azurerm_subnet.pr-subnet6[0].id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_network_security_group_association" "pr-subnet7" {
  count                     = var.private-subnet7 == null ? 0 : 1
  subnet_id                 = azurerm_subnet.pr-subnet7[0].id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}
resource "azurerm_subnet_network_security_group_association" "pr-subnet8" {
  count                     = var.private-subnet8 == null ? 0 : 1
  subnet_id                 = azurerm_subnet.pr-subnet8[0].id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}
resource "azurerm_subnet_network_security_group_association" "pr-subnet9" {
  count                     = var.private-subnet9 == null ? 0 : 1
  subnet_id                 = azurerm_subnet.pr-subnet9[0].id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}
resource "azurerm_subnet_network_security_group_association" "pr-subnet10" {
  count                     = var.private-subnet10 == null ? 0 : 1
  subnet_id                 = azurerm_subnet.pr-subnet10[0].id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}
####NAT GATEWAY###########
resource "azurerm_public_ip" "nat-gateway" {
  name                = var.natgateway_publicip
  location            = var.vnet_location
  resource_group_name = var.vnet_resource_group
  allocation_method   = "Static"
  sku                 = "Standard"
  zones               = ["1"]
  tags = {
    env          = var.environment
    team         = var.team
    owner        = var.owner
    owneremail   = var.owneremail
    creationdate = var.creationdate
    renewaldate  = var.renewaldate
    reason       = var.reason
    component    = var.component
    module       = var.module
  }
}

resource "azurerm_public_ip_prefix" "example" {
  name                = var.natgateway_publicip_prefix
  location            = var.vnet_location
  resource_group_name = var.vnet_resource_group
  prefix_length       = 31
  zones               = ["1"]
  tags = {
    env          = var.environment
    team         = var.team
    owner        = var.owner
    owneremail   = var.owneremail
    creationdate = var.creationdate
    renewaldate  = var.renewaldate
    reason       = var.reason
    component    = var.component
    module       = var.module
  }
}


resource "azurerm_nat_gateway" "my_gateway" {
  name                    = var.gateway_name
  location                = var.vnet_location
  resource_group_name     = var.vnet_resource_group
  idle_timeout_in_minutes = 30
  sku_name                = "Standard"
  # public_ip_address_ids = [
  #   azurerm_public_ip.nat-gateway.id,
  #   azurerm_public_ip_prefix.example.id
  # ]
  zones = ["1"]
  tags = {
    env          = var.environment
    team         = var.team
    owner        = var.owner
    owneremail   = var.owneremail
    creationdate = var.creationdate
    renewaldate  = var.renewaldate
    reason       = var.reason
    component    = var.component
    module       = var.module
  }
}

resource "azurerm_nat_gateway_public_ip_association" "example" {
  nat_gateway_id       = azurerm_nat_gateway.my_gateway.id
  public_ip_address_id = azurerm_public_ip.nat-gateway.id
}

resource "azurerm_nat_gateway_public_ip_prefix_association" "azurerm_nat_gateway_public_ip_prefix_association" {
  nat_gateway_id      = azurerm_nat_gateway.my_gateway.id
  public_ip_prefix_id = azurerm_public_ip_prefix.example.id
}
resource "azurerm_subnet_nat_gateway_association" "pr-subnet1" {
  subnet_id      = azurerm_subnet.pr-subnet1.id
  nat_gateway_id = azurerm_nat_gateway.my_gateway.id
}

resource "azurerm_subnet_nat_gateway_association" "pr-subnet2" {
  subnet_id      = azurerm_subnet.pr-subnet2.id
  nat_gateway_id = azurerm_nat_gateway.my_gateway.id
}

resource "azurerm_subnet_nat_gateway_association" "pr-subnet3" {
  subnet_id      = azurerm_subnet.pr-subnet3.id
  nat_gateway_id = azurerm_nat_gateway.my_gateway.id
}

resource "azurerm_subnet_nat_gateway_association" "pr-subnet6" {
  count          = var.private-subnet6 == null ? 0 : 1
  subnet_id      = azurerm_subnet.pr-subnet6[0].id
  nat_gateway_id = azurerm_nat_gateway.my_gateway.id
}

resource "azurerm_subnet_nat_gateway_association" "pr-subnet7" {
  count          = var.private-subnet7 == null ? 0 : 1
  subnet_id      = azurerm_subnet.pr-subnet7[0].id
  nat_gateway_id = azurerm_nat_gateway.my_gateway.id
}
resource "azurerm_subnet_nat_gateway_association" "pr-subnet10" {
  count          = var.private-subnet10 == null ? 0 : 1
  subnet_id      = azurerm_subnet.pr-subnet10[0].id
  nat_gateway_id = azurerm_nat_gateway.my_gateway.id
}
resource "azurerm_subnet" "subnet"  {
  count               = length(var.subnet_names)
  name                = var.subnet_names[count.index]
  resource_group_name = var.vnet_resource_group
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.subnet_cidr[count.index]]
}


resource "azurerm_subnet_network_security_group_association" "security_group_association" {
  count                   = length(var.subnet_names)
  subnet_id               = azurerm_subnet.subnet[count.index].id
  network_security_group_id = azurerm_network_security_group.pr-nat.id
}

resource "azurerm_subnet_nat_gateway_association" "nat_gateway_association" {
  count                   = length(var.subnet_names)
  subnet_id               = azurerm_subnet.subnet[count.index].id
  nat_gateway_id          = var.nat2_gateway_id != "" ? var.nat2_gateway_id : azurerm_nat_gateway.my_gateway.id
}
