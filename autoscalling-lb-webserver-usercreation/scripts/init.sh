sudo apt-get install nginx -y
sudo bash -c 'echo \"server { listen 8080; location / { root /var/www/html; index index.html; } }\" > /etc/nginx/sites-available/default
sudo service nginx restart'
sudo useradd -m -s /bin/bash myusername
mkdir -p /home/myusername/.ssh
echo 'ssh-rsa <YOUR_PUBLIC_KEY>' | sudo tee /home/myusername/.ssh/authorized_keys
sudo chown -R myusername:myusername /home/myusername/.ssh
sudo chmod 700 /home/myusername/
sudo chmod 600 /home/myusername/.ssh
sudo bash -c 'echo \"myusername ALL=(ALL) NOPASSWD: /usr/sbin/service nginx restart\" >> /etc/sudoers.d/myusername'"
