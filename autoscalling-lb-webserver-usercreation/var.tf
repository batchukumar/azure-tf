variable "location" {
  type    = string
  default = "East US"
}

variable "resource_group_name" {
  type    = string
  default = "dev"
}

variable "saname1" {
  type    = string
  default = "example-storage"
}

variable "saname" {
  type    = string
  default = "somestorageaccount12354"
}


variable "capacity" {
  type    = number
  default = 2
}

variable "subnet_id" {
  type    = string
  default = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/dev/providers/Microsoft.Network/virtualNetworks/dev/subnets/public-subnet1"
}
variable "nat2_gateway_id" {
  type= string
  default=""
  
}