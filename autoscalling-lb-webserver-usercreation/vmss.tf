resource "azurerm_public_ip" "publicip1" {
  name                = "publicip1"
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  #  domain_name_label   = var.resource_group_name
}

resource "azurerm_lb" "loadbalancer" {
  name                = "${var.resource_group_name}-lb"
  location            = var.location
  resource_group_name = var.resource_group_name

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.publicip1.id
  }
}

resource "azurerm_lb_backend_address_pool" "bepool" {
  loadbalancer_id = azurerm_lb.loadbalancer.id
  name            = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "health-probe" {
  name            = "http-probe"
  loadbalancer_id = azurerm_lb.loadbalancer.id
  protocol        = "Http"
  request_path    = "/index.html"
  port            = 80
}

resource "azurerm_lb_rule" "lbrulehttp" {
  loadbalancer_id                = azurerm_lb.loadbalancer.id
  name                           = "LBRuleHTTP"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 8080
  frontend_ip_configuration_name = "PublicIPAddress"
  probe_id                       = azurerm_lb_probe.health-probe.id
  # backend_address_pool_id        = azurerm_lb_backend_address_pool.bepool.id
}

resource "azurerm_lb_nat_pool" "lbnatpoolssh" {
  name                           = "ssh"
  resource_group_name            = var.resource_group_name
  loadbalancer_id                = azurerm_lb.loadbalancer.id
  protocol                       = "Tcp"
  frontend_port_start            = 50000
  frontend_port_end              = 50119
  backend_port                   = 8080
  frontend_ip_configuration_name = "PublicIPAddress"
}

resource "azurerm_storage_account" "storage-account" {
  name                     = var.saname
  location                 = var.location
  resource_group_name      = var.resource_group_name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "storage-container" {
  name                  = "vhds"
  storage_account_name  = azurerm_storage_account.storage-account.name
  container_access_type = "private"
}

resource "azurerm_virtual_machine_scale_set" "vmss" {
  name                = "${var.resource_group_name}-vmss"
  location            = var.location
  resource_group_name = var.resource_group_name

  upgrade_policy_mode = "Manual"
  overprovision       = false

  sku {
    name     = "Standard_F2"
    tier     = "Standard"
    capacity = var.capacity
  }

  os_profile {
    computer_name_prefix = "${var.resource_group_name}-vm"
    admin_username       = "superuser"
    admin_password       = "Admin1234!"
    custom_data          = base64encode(file("scripts/init.sh"))
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_profile_os_disk {
    name           = "osDiskProfile"
    caching        = "ReadWrite"
    create_option  = "FromImage"
    vhd_containers = ["${azurerm_storage_account.storage-account.primary_blob_endpoint}${azurerm_storage_container.storage-container.name}"]
  }

  storage_profile_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  network_profile {
    name    = "terraformnetworkprofile"
    primary = true

    ip_configuration {
      name                                           = "TestIPConfiguration"
      primary                                        = true
      subnet_id                                      = var.subnet_id
      load_balancer_backend_address_pool_ids         = [azurerm_lb_backend_address_pool.bepool.id]
      load_balancer_inbound_nat_rules_ids            = [azurerm_lb_nat_pool.lbnatpoolssh.id]
    }
  }
}
