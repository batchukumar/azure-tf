terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.92.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "dev"
    storage_account_name = "terraformstate"
    container_name       = "devops"
    key                  = "terraform.vnet.tfstate"
  }
}
