variable "environment" {
  description = "environment value"
  default     = "test"
}

variable "gateway_name" {
  description = "team value"
  default     = "test-natgateway"
}

variable "team" {
  description = "team value"
  default     = "cot"
}

variable "vnet_name" {
  description = "The resource group where the VNet will be created"
  type        = string
  default     = "test"
}

variable "vnet_resource_group" {
  description = "The resource group where the VNet will be created"
  default     = "test"
}

variable "vnet_location" {
  description = "The location VNet will be created"
  default     = "East US"
}

variable "vnet_address_space" {
  description = "The location VNet will be created"
  default     = ["10.110.0.0/16"]
}

variable "public-subnet1" {
  description = "The public-subnet1 CIDR VNet will be created"
  default     = ["10.110.4.0/22"]
}

variable "public-subnet2" {
  description = "The The public-subnet2 CIDR will be created"
  default     = ["10.110.8.0/22"]
}

variable "public-subnet3" {
  description = "The The public-subnet3 CIDR will be created"
  default     = ["10.110.12.0/22"]
}

	variable "public-subnet4" {	
  description = "The The public-subnet3 CIDR will be created"	
  default     = ["10.110.36.0/24"]	
}

variable "private-subnet1" {
  description = "The private-subnet1 CIDR VNet will be created"
  default     = ["10.110.16.0/22"]
}

variable "private-subnet2" {
  description = "The The private-subnet2 CIDR will be created"
  default     = ["10.110.20.0/22"]
}

variable "private-subnet3" {
  description = "The The public-subnet3 CIDR will be created"
  default     = ["10.110.24.0/22"]
}

variable "private-subnet4" {
  description = "The The private-subnet4 CIDR will be created"
  default     = ["10.110.28.0/22"]
}

variable "private-subnet5" {
  description = "The The public-subnet5 CIDR will be created"
  default     = ["10.110.32.0/22"]
}

	variable "private-subnet9" {	
  description = "The The public-subnet9 CIDR will be created"	
  default     = ["10.110.37.0/24"]	
}


variable "public-subnet-nsg" {
  description = "public subnet nsg"
  default     = "nat-nsg"
}

variable "private-subnet-nsg" {
  description = "private subnet nsg"
  default     = "nat-pr-nsg"
}

variable "natgateway_publicip" {
  description = "private subnet nsg"
  default     = "nat-gateway-public-ip"
}

variable "natgateway_publicip_prefix" {
  description = "private subnet nsg"
  default     = "nat-gateway-publicIP-Prefix"
}

variable "component" {
  description = "component"
  default     = "vnet"
}

variable "creationdate" {
  description = "creationdate"
  default     = "Jun022023"
}

variable "owner" {
  description = "owner"
  default     = "user"
}

variable "owneremail" {
  description = "owneremail"
  default     = "user@mail.com"
}

variable "renewaldate" {
  description = "renewaldate"
  default     = "Jun022024"
}

variable "reason" {
  description = "reason"
  default     = "new infra setup"
}

variable "module" {
  description = "module"
  default     = "vnet"
}

variable "resource_group_name" {
  description = "resource group name"
  default     = "test"
}

variable "nat2_gateway_id " {
  type=string
  default=""
  
}